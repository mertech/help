$(function () {
  var link_handler = function (e) {
    window.open(e.data.link, "_self");
  };

  const data = [
    {
      title: "База знаний MERTECH",
      description: "Частые вопросы об обслуживании оборудования\n" +
          "и программного обеспечения MERTECH и ответы на них.",
      link: "",
      items: [
        {
          title: "Весы автономные",
          text: " ",
          link: "scales_autonomous",
          icon: "category/vesy-avtonom.png",
          enabled: "true",
          child: [
            {
              title: "Как убрать точку на индикации «СТОИМОСТЬ» для весов M-ER 326 AC, 327",
              text: " ",
              link: "/scales_autonomous/Как_убрать_точку_326.html",
              enabled: "true",
            },
            {
              title: "Как убрать точку на индикации «СТОИМОСТЬ» для весов M-ER 328",
              text: " ",
              link: "/scales_autonomous/Как_убрать_точку_328.html",
              enabled: "true",
            },
            {
              title: "Переключение единиц измерения (кг и фунты) в весах M-er 328,333 (с сенсорным типом управления),335 (в определённой ревизии системной платы)",
              text: " ",
              link: "/scales_autonomous/Переключение_единиц_измерения.html",
              enabled: "true",
            },
            {
              title: "Отключение единиц измерений в весах 326AFU",
              text: " ",
              link: "/scales_autonomous/Отключение_единиц_измерений_в_весах_326AFU.html",
              enabled: "true",
            },
            {
              title: "Изменение параметров подсветки и режима энергосбережения весов с LCD и LED подсветкой M-er 223, 328,329, 333, 335",
              text: " ",
              link: "/scales_autonomous/Изменение_параметров_подсветки.html",
              enabled: "true",
            },
            {
              title: "M-ER 328,333 инициализация сенсорной клавиатуры при неработоспособности клавиш",
              text: " ",
              link: "/scales_autonomous/M_ER_328_333_инициализация.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Весы интерфейсные",
          text: " ",
          link: "scales_interface",
          icon: "category/vesy-interface.png",
          enabled: "true",
          child: [
            {
              title: "Изменение протокола передачи данных в весах M-er 326",
              text: " ",
              link: "/scales_interface/Изменение_протокола_326.html",
              enabled: "true",
            },
            {
              title: "Изменение протокола передачи данных в весах M-er 328",
              text: " ",
              link: "/scales_interface/Изменение_протокола_328.html",
              enabled: "true",
            },
            {
              title: "Создание драйвера и оборудования в 1С",
              text: " ",
              link: "/scales_interface/Создание_драйвера.html",
              enabled: "true",
            },
            {
              title: "Установка драйвера весов в 1С Розница редакции 2.3.9.23 и выше",
              text: " ",
              link: "/scales_interface/Установка_драйвера_розница.html",
              enabled: "true",
            },
            {
              title: "Установка системной библиотеки MerCas для весов M-ER в ОС Windows",
              text: " ",
              link: "/scales_interface/Установка_системной.html",
              enabled: "true",
            },
            {
              title: "Схема распайки кабеля последовательного порта RS232",
              text: " ",
              link: "/scales_interface/Схема_распайки.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Весы с печатью",
          text: " ",
          link: "scales_print",
          icon: "category/vesy-s-pechat.png",
          enabled: "true",
          child: [
            {
              title: "Варианты загрузки картинок товаров в весы",
              text: " ",
              link: "/scales_print/варианты_загрузки_картинок.html",
              enabled: "true",
            },
            {
              title: "При выгрузке из 1С цены уменьшаются в 100 раз",
              text: " ",
              link: "/scales_print/При_выгрузке_из_1С.html",
              enabled: "true",
            },
            {
              title: "Сброс весов до заводских настроек",
              text: " ",
              link: "/scales_print/725_reset_closed.html",
              enabled: "true",
            },
            {
              title: "Устранение проблемы с показанием массы",
              text: " ",
              link: "/scales_print/Устранение_проблемы_с_показанием_массы.html",
              enabled: "true",
            },
            {
              title: "Основные разъемы и платы весов с печатью этикеток.",
              text: " ",
              link: "/scales_print/725_foto_connect_closed.html",
              enabled: "true",
            },
            {
              title: "Скрипты для взаимодействия с весами через ADB",
              text: " ",
              link: "/scales_print/725_scripts_closed.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Сканеры штрихкодов",
          text: " ",
          link: "barcode_scanners",
          icon: "category/scanery.png",
          enabled: "true",
          child: [
            {
              title: "GS разделитель для проводных сканеров",
              text: " ",
              link: "/barcode_scanners/SUPERLEAD/GS_provod.html",
              enabled: "true",
            },
            {
              title: "GS разделитель для беспроводных сканеров",
              text: " ",
              link: "/barcode_scanners/SUPERLEAD/GS_bez_provod.html",
              enabled: "true",
            },
            {
              title: "Лидирующий символ (UPC-E)",
              text: " ",
              link: "/barcode_scanners/SUPERLEAD/лидирующий_символ.html",
              enabled: "true",
            },
            {
              title: "F1-F12 Префиксы и суффиксы",
              text: " ",
              link: "/barcode_scanners/SUPERLEAD/суффиксы_и_префиксы_Ф.html",
              enabled: "true",
            },
            {
              title: "Чтение символов Cyrilic - для проводного сканера",
              text: " ",
              link: "/barcode_scanners/SUPERLEAD/чтение_cyrilic_провод.html",
              enabled: "true",
            },
            {
              title: "Чтение символов Cyrilic - для беспроводного сканера",
              text: " ",
              link: "/barcode_scanners/SUPERLEAD/чтение_cyrilic.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Терминалы сбора данных",
          text: " ",
          link: "tsd",
          icon: "category/tsd.png",
          enabled: "true",
          child: [
            {
              title: "Seuic AutoID LIM Включение разрешения установки приложений из неизвестных источников",
              text: " ",
              link: "/tsd/Mertech_Seuic/neizvestnye_istochniki_LIM.html",
              enabled: "true",
            },
            {
              title: "Утилита для настройки Seuic Q9C",
              text: " ",
              link: "/tsd/Mertech_Seuic/utilita_nastroyki_Q9C.html",
              enabled: "true",
            },
            {
              title: "Прошивка Mertech Seuic Q9C",
              text: " ",
              link: "/tsd/Mertech_Seuic/proshivka_seuic_q9c.html",
              enabled: "true",
            },
            {
              title: "Первоначальная настройка Seuic Q9C после сброса",
              text: " ",
              link: "/tsd/Mertech_Seuic/pervaya_nastroika_q9c.html",
              enabled: "true",
            },
            {
              title: "Подсветка сканера Mertech Seuic AutoID Q9C",
              text: " ",
              link: "/tsd/Mertech_Seuic/podsvetka_skanera_q9c.html",
              enabled: "true",
            },
            {
              title: "Отключение передачи «F12» в 1С УТ 11 при подключении по RDP",
              text: " ",
              link: "/tsd/Mertech_Seuic/LIM_F12_1C_closed.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Принтеры этикеток",
          text: " ",
          link: "label_printers",
          icon: "category/printery-et.png",
          enabled: "true",
          child: [
            {
              title: "Установка драйвера для принтеров этикеток Mprint в ОС Windows",
              text: " ",
              link: "https://help.mertech.ru/label_printers/Общее/Установка_драйвера_этикетки.html",
              enabled: "true",
            },
            {
              title: "Установка пакета драйверов для принтеров этикеток Mprint в ОС linux (Ubuntu)",
              text: " ",
              link: "https://help.mertech.ru/label_printers/Общее/Установка_пакета_драйверов_для_принтеров.html",
              enabled: "true",
            },
            {
              title: "LP58EVA Настройка датчика зазора этикеток.",
              text: " ",
              link: "https://help.mertech.ru/label_printers/LP58_и_LP80_EVA/LP58EVA_Настройка_датчика_зазора_этикеток.html",
              enabled: "true",
            },
            {
              title: "СХЕМА распиновки кабеля RS232_RJ11.",
              text: " ",
              link: "https://help.mertech.ru/label_printers/LP58_и_LP80_EVA/СХЕМА_распиновки_кабеля_RS232_RJ11.html",
              enabled: "true",
            },
            {
              title: "LP80 TERMEX после обновления Windows 10 не печатает",
              text: " ",
              link: "https://help.mertech.ru/label_printers/LP58_и_LP80_EVA/termex_ne_pechataet_closed.html",
              enabled: "true",
            },
            {
              title: "TLP-100 и TLP-300 рвётся риббон или неудовлетворительное качество печати.",
              text: " ",
              link: "https://help.mertech.ru/label_printers/TLP-100_и_TLP-300/ribbon_hot.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Принтеры чеков",
          text: " ",
          link: "receipt_printers",
          icon: "category/print-chek.png",
          enabled: "true",
          child: [
            {
              title: "Установка кириллической кодировки в чековом принтере G80.",
              text: " ",
              link: "https://help.mertech.ru/receipt_printers/Mprint_G80/set_cyrillic.html",
              enabled: "true",
            },
            {
              title: "Устранение печати иероглифов.",
              text: " ",
              link: "https://help.mertech.ru/receipt_printers/Mprint_G80/Устранение_печати%20_иероглифов.html",
              enabled: "true",
            },
            {
              title: "Настройка Wi-Fi модуля",
              text: " ",
              link: "https://help.mertech.ru/receipt_printers/Mprint_G80/Настройка_Wi-Fi_модуля_closed.html",
              enabled: "true",
            },
            {
              title: "Wi-Fi модуль не подключается к сети.",
              text: " ",
              link: "https://help.mertech.ru/receipt_printers/Mprint_G80/Wi-Fi_not_connect.html",
              enabled: "true",
            },
            {
              title: "Распиновка COM кабеля для Q80",
              text: " ",
              link: "https://help.mertech.ru/receipt_printers/Mprint_Q80/Распиновка_COM_кабеля_для_Q80.html",
              enabled: "true",
            },
            {
              title: "Q80 Уменьшение пустого пространства внизу чека",
              text: " ",
              link: "https://help.mertech.ru/receipt_printers/Mprint_Q80/Q80_Уменьшение_пустого_пространства_внизу_чека.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Принтеры мобильные",
          text: " ",
          link: "mobile_printers",
          icon: "category/mob-printery.png",
          enabled: "true",
          child: [
            {
              title: "Загрузка шрифтов на Mertech Alpha (T300PRO)",
              text: " ",
              link: "https://help.mertech.ru/mobile_printers/Мобильный_принтер_ALPHA/Загрузка_шрифтов_на_Mertech_Alpha_T300PRO.html",
              enabled: "true",
            },
            {
              title: "Настройка подключения принтера через Bluetooth соединение",
              text: " ",
              link: "https://help.mertech.ru/mobile_printers/Мобильный_принтер_ALPHA/Настройка_подключения_через_Bluetooth_closed.html",
              enabled: "true",
            },
            {
              title: "Печать через Wi-Fi",
              text: " ",
              link: "https://help.mertech.ru/mobile_printers/Мобильный_принтер_ALPHA/Печать_через_Wi-Fi.html",
              enabled: "true",
            },
            {
              title: "Подключение к ТСД",
              text: " ",
              link: "https://help.mertech.ru/mobile_printers/Мобильный_принтер_ALPHA/Подключение_к_ТСД.html",
              enabled: "true",
            },
            {
              title: "Обновление прошивки на Mertech Alpha (T300PRO)",
              text: " ",
              link: "https://help.mertech.ru/mobile_printers/Мобильный_принтер_ALPHA/Обновление_прошивки_на_Mertech_Alpha_T300PRO.html",
              enabled: "true",
            },
            {
              title: "Смена типа бумаги без утилиты",
              text: " ",
              link: "https://help.mertech.ru/mobile_printers/Мобильный_принтер_ALPHA/Смена_типа_бумаги_без_утилиты.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Детекторы банкнот",
          text: " ",
          link: "banknote_detectors",
          icon: "category/detectoty.png",
          enabled: "true",
          child: [
            {
              title: "Расположение датчиков D20 Promatic LED-TFT ошибки самотестирования",
              text: " ",
              link: "https://help.mertech.ru/banknote_detectors/D20_Promatic_LED-TFT/Расположение_датчиков_D20_Promatic_LED-TFT_ошибки_самотестирования.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Счетчики банкнот",
          text: " ",
          link: "banknote_counters",
          icon: "category/schetchiki.png",
          enabled: "true",
          child: [
            {
              title: "Замена батарейки и Платы индикации Счётчика банкнот С-100",
              text: " ",
              link: "https://help.mertech.ru/banknote_counters/C-100/zamena_batareyki_C100_closed.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "QR дисплей",
          text: " ",
          link: "qr_display",
          icon: "category/cat-sbp.png",
          enabled: "true",
          child: [
            {
              title: "QR-дисплей, подключение к системе 1С-Предприятие",
              text: " ",
              link: "https://help.mertech.ru/qr_display/QR-дисплей_подключение_к_системе_1С-Предприятие.html",
              enabled: "true",
            },
            {
              title: "Как сделать выбор нескольких приложений оплаты для Android устройств при оплате с QR дисплея",
              text: " ",
              link: "https://help.mertech.ru/qr_display/Как_сделать_выбор_нескольких_приложений_оплаты_для_Android_устройств_при_оплате_с_QR_дисплея.html",
              enabled: "true",
            },
            {
              title: "Обновление прошивки MERTECH QR дисплей",
              text: " ",
              link: "https://help.mertech.ru/qr_display/QR_update.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Прайсчекеры",
          text: " ",
          link: "pricecheckers",
          icon: "category/price-check.png",
          enabled: "true",
          child: [
            {
              title: "Киоск. Настройка приложений для работы в режиме киоска.",
              text: " ",
              link: "https://help.mertech.ru/pricecheckers/Mertech_P8/Киоск._Настройка_приложений_для_работы_в_режиме_киоска.html",
              enabled: "true",
            },
            {
              title: "Изменение скорости передачи данных со сканера Henex.",
              text: " ",
              link: "https://help.mertech.ru/pricecheckers/Mertech_P8/Изменение_скорости_передачи_данных_со_сканера_Henex.html",
              enabled: "true",
            },
            {
              title: "Прошивка Mertech P8",
              text: " ",
              link: "https://help.mertech.ru/pricecheckers/Mertech_P8/Прошивка_Mertech_P8_closed.html",
              enabled: "true",
            },
            {
              title: "Полоса в нижней части экрана",
              text: " ",
              link: "https://help.mertech.ru/pricecheckers/Mertech_P8/Полоса_в_нижней_части_экрана_closed.html",
              enabled: "true",
            },
          ]
        },
        {
          title: "Заметки на полях",
          text: " ",
          link: " ",
          icon: "doc@3x.png",
          enabled: "false",
        },
      ],
    }
  ];

  var commingSectionText = "В процессе разработки";
  var c_cont = $("#accordion");
  for (var i = 0; i < data.length; i++) {
    var doc = data[i];
    let d_col = $("<div>", {
      id: "heading" + i,
      class:
        "col-sm-12 col-md-12 col-xl-10 d-flex align-items-stretch pr-2 pl-2 pb-3",
      "data-toggle": "collapse",
      "data-target": "#collapse_false_" + i,
      "aria-expanded": "true",
      "aria-controls": "collapse_false_" + i,
    });
    let d_card = $("<div>", { class: "card w-100" });
    let d_card_body = $("<div>", { class: "card-body" });
    let d_card_h = $("<h5>", { class: "card-title d-inline-block" });

    d_card_h.text(doc.title);
    d_card_h.appendTo(d_card_body);
    $('<div class="description_tab"><p>'+doc.description+'</p></div>').appendTo(d_card_body);
    d_card_body.appendTo(d_card);
    d_card.appendTo(d_col);
    d_col.appendTo(c_cont);

    let d_coll_div = $("<div>", {
      id: "collapse" + i,
      class: "collapse show",
      "aria-labelledby": "heading" + i,
      "data-parent": "#accordion",
    });
    d_coll_div.appendTo(c_cont);

    var itms_cont = $("#collapse" + i);
    var itms = doc.items;
    for (var j = 0; j < itms.length; j++) {
      var comming = " (" + commingSectionText + ")";
      var card_info = itms[j];
      var cursor_class = "";
      if (card_info.enabled != "true") {
        card_info.title += comming;
        card_info.icon = "lock@3x.png";
        cursor_class = "block-cursor";
      }
      let b_col = $("<div>", {
        class:
          "col-sm-12 col-md-6 col-xl-4 d-flex align-items-stretch pr-2 pl-2 pb-3",
      });
      let b_card = $("<div>", { class: "card-light w-100 " + cursor_class });
      let b_card_body = $("<div>", { class: "card-body" });
      let b_card_h = $("<div>", { class: "card-light-title d-inline-block " });



      /**
       * Выводим ссылки на детальные статьи раздела
       */
      if(card_info.child){
        b_card_body.addClass('detail_wrapper');
        let card_icon = '';
        if (card_info.icon.length) {
          let image_path = "";
          image_path += "img/" + card_info.icon;
          card_icon = "<img src='"+image_path+"' class='icon_category'>";
        }

        let html_detail_links = '<a class="btn btn-primary collapsed" data-toggle="collapse" href="#detail_links_'+j+'" role="button" aria-expanded="false" aria-controls="detail_links_'+j+'">'+card_icon+' <span>'+  card_info.title + '</span></a>';
        html_detail_links += '<div class="collapse detail_links_wrapper" id="detail_links_'+j+'">\n' +
            '  <div class="card card-body">\n' +
            '    <ul>';

        $.each(card_info.child,function(index,child_value){
          html_detail_links += '<li><a href="'+child_value.link+'">'+child_value.title+'</a></li>';
        });
        html_detail_links += '</ul>';

        html_detail_links += '<p class="more_wrapper"><a href="'+card_info.link+'" class="btn btn-primary">Смотреть все</a></p></div></div>';

        $(html_detail_links).appendTo(b_card_h);


      }else{
        /**
         * По старинке показываем
         */
        b_card_body.addClass('detail_one_wrapper');
        b_card_h.text(card_info.title);

        if (card_info.icon.length) {
          var image_path = "";
          image_path += "img/" + card_info.icon;
          var card_icon = $("<img>", {
            class: "d-inline-block icon_category",
            height: "32px",
            width: "32px",
            src: image_path,
          });
          card_icon.appendTo(b_card_body);
        }
      }



      b_card_h.appendTo(b_card_body);
      b_card_body.appendTo(b_card);
      b_card.appendTo(b_col);

      /*if (card_info.icon.length) {
        var image_path = "";
        image_path += "img/" + card_info.icon;
        var card_icon = $("<img>", {
          class: "d-inline-block float-right",
          height: "32px",
          width: "32px",
          src: image_path,
        });
        card_icon.appendTo(b_card_body);
      }*/

      /*var card_text = $("<p>", { class: "card-light-text" }).text(
        card_info.text
      );
      card_text.appendTo(b_card_body);*/

      b_col.appendTo(itms_cont);

      if (!card_info.child && card_info.link && card_info.enabled == "true") {
        b_card.click({ link: doc.link + "/" + card_info.link }, link_handler);
      }
    }
  }
});
